using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using JustEat.TechTest.Api.Exceptions;
using JustEat.TechTest.Api.Models;
using Moq;
using NUnit.Framework;
using RestSharp;
using RestSharp.Authenticators;

namespace JustEat.TechTest.Api.Tests.UnitTests
{
    [TestFixture]
    public class JustEatApiClientScenarios
    {
        RestSharpJustEatApiClient SystemUnderTest { get; set; }
        Mock<IRestClient> MockRestClient { get; set; }
        Mock<IAuthenticator> MockAuthenticator { get; set; }

        [SetUp]
        public void Setup()
        {
            MockRestClient = new Mock<IRestClient>();
            MockAuthenticator = new Mock<IAuthenticator>();

            MockRestClient.Setup(r => r.DefaultParameters).Returns(new List<Parameter>());

            SystemUnderTest = new RestSharpJustEatApiClient(MockRestClient.Object,  MockAuthenticator.Object, new Uri("http://test.com"), "en-GB", "uk");
        }

        #region Test Cases
        [Test]
        [Description("When we construct the class the rest client should have a default Accept-Language header.")]
        public void Constructor_SetsRestClientAcceptLanguage()
        { 
            MockRestClient.Object.DefaultParameters.FirstOrDefault(p => 
                p.Type == ParameterType.HttpHeader 
                && p.Name.Equals("Accept-Language")
                && p.Value.Equals("en-GB"))
                .Should()
                .NotBeNull();

        }

        [Test]
        [Description("When we construct the class, the rest client should have a default Accept-Tenant header.")]
        public void Constructor_SetsRestClientAcceptTenant()
        {
            MockRestClient.Object.DefaultParameters.FirstOrDefault(p => 
                p.Type == ParameterType.HttpHeader 
                && p.Name.Equals("Accept-Tenant") 
                && p.Value.Equals("uk"))
                .Should()
                .NotBeNull();
        }

        [Test]
        [Description("When we call GetRestaurants, assuming that we have a valid http response, we should get the list of restaurants.")]
        public async Task GetRestaurants_ValidResponse_ReturnsListOfRestaurants()
        {
            var restaurants = new RestaurantsCollection();;
            var response = new Mock<IRestResponse<RestaurantsCollection>>();

            response.Setup(r => r.Data).Returns(restaurants);
            MockRestClient.Setup(r => r.ExecuteTaskAsync<RestaurantsCollection>(It.IsAny<IRestRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(response.Object);

            var responseActual = await SystemUnderTest.GetRestaurantsAsync("SE13");

            responseActual.Should().BeSameAs(restaurants);
        }

        [Test]
        [Description("When the rest client contains an aception, we should throw an exception.")]
        public void GetRestaurants_RestClientThrowsException_ThrowJustEatApiException()
        {
            var response = new Mock<IRestResponse<RestaurantsCollection>>();

            response.Setup(r => r.ErrorException).Returns(new Exception());
            MockRestClient.Setup(r => r.ExecuteTaskAsync<RestaurantsCollection>(It.IsAny<IRestRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(response.Object);
            
            Assert.ThrowsAsync<JustEatApiException>(() => SystemUnderTest.GetRestaurantsAsync("SE13"));
        }

        [Test]
        [Description("When the http response is = 400-499, the api should throw an exception.")]
        public void GetRestaurants_RestClientBadStatusCode_ThrowClientJustEatApiException()
        {
            var response = new Mock<IRestResponse<RestaurantsCollection>>();

            response.Setup(r => r.StatusCode).Returns(HttpStatusCode.Forbidden);
            MockRestClient.Setup(r => r.ExecuteTaskAsync<RestaurantsCollection>(It.IsAny<IRestRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(response.Object);

            Assert.ThrowsAsync<ClientJustEatApiException>(() => SystemUnderTest.GetRestaurantsAsync("SE13"));
        }

        [Test]
        [Description("When the http response is = 500-599, the api should throw an exception.")]
        public void GetRestaurants_RestClientInternalServerErrorCode_ThrowClientJustEatApiException()
        {
            var response = new Mock<IRestResponse<RestaurantsCollection>>();

            response.Setup(r => r.StatusCode).Returns(HttpStatusCode.InternalServerError);
            MockRestClient.Setup(r => r.ExecuteTaskAsync<RestaurantsCollection>(It.IsAny<IRestRequest>(), It.IsAny<CancellationToken>())).ReturnsAsync(response.Object);

            Assert.ThrowsAsync<ServerJustEatApiException>(() => SystemUnderTest.GetRestaurantsAsync("SE13"));
        }
        #endregion
    }
}
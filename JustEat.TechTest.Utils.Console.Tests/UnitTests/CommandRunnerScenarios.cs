﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using JustEat.TechTest.ConsoleUtils.Commands;
using Moq;
using NUnit.Framework;

namespace JustEat.TechTest.Utils.Console.Tests.UnitTests
{
    [TestFixture]
    public class CommandRunnerScenarios
    {
        CommandRunner SystemUnderTest { get; set; }
        Mock<ICommandRunnerPresenter> MockOutputHandler { get; set; }


        [SetUp]
        public void Setup()
        {
            MockOutputHandler = new Mock<ICommandRunnerPresenter>();
            SystemUnderTest = new CommandRunner(MockOutputHandler.Object);
        }

        #region Test Cases
        [Test]
        [Description("When we have a a command group and a command, and the user requests the command, the command should be processed")]
        public async Task Run_CommandGroupSingleChild_Processes()
        {
            //Given that we have a command group with a single child command
            var rootCommandGroup = new Mock<ICommandGroup>();
            var command = new Mock<ICommand>();

            command.Setup(p => p.CommandName).Returns("test");
            rootCommandGroup.Setup(p => p.Children).Returns(new List<ICommand>()
            {
                command.Object
            });

            //When we request the child command
            await SystemUnderTest.Run(new[] { "test" }, rootCommandGroup.Object);

            //The command should be processed
            command.Verify(c => c.Process(It.IsAny<string[]>(), It.IsAny<CancellationToken>()));
        }

        [Test]
        [Description("When we have a single command, regardless of the input the command should be processed.")]
        public async Task Run_SingleCommand_Processes()
        {
            //Given that the root is a command
            var command = new Mock<ICommand>();

            //when we run processed the command with no arguments
            await SystemUnderTest.Run(new string[] {}, command.Object);

            //the command should be processed
            command.Verify(c => c.Process(It.IsAny<string[]>(), It.IsAny<CancellationToken>()));
        }

        [Test]
        [Description("If we have a command group but no children, and the use requests a command, a list of sub commands should be printed on screen.")]
        public async Task Run_SubCommandMissing_ListValidSubCommands()
        {
            //given a command group with no children
            var rootCommandGroup = new Mock<ICommandGroup>();

            rootCommandGroup.Setup(p => p.Children).Returns(new List<ICommand>());

            //when a sub command is run that doesnt exist
            await SystemUnderTest.Run(new[] { "test" }, rootCommandGroup.Object);

            //We should render the list of commands and a message
            MockOutputHandler.Verify(c => c.CommandNotFound("test"));
            MockOutputHandler.Verify(c => c.ListSubCommands(rootCommandGroup.Object));
        }

        [Test]
        [Description("If the user has selected a subcommand then list its children.")]
        public async Task Run_CommandGroupSelected_ListsSubCommands()
        {
            //given a set of command groups
            var rootCommandGroup = new Mock<ICommandGroup>();
            var subCommand = new Mock<ICommandGroup>();

            subCommand.Setup(p => p.CommandName).Returns("test");
            rootCommandGroup.Setup(p => p.Children).Returns(new List<ICommand>()
            {
                subCommand.Object
            });

            //when a user selects a command group (eg. je restaurants) 
            await SystemUnderTest.Run(new[] { "test" }, rootCommandGroup.Object);
            
            //we should list the avaliable sub commands.
            MockOutputHandler.Verify(c => c.ListSubCommands(subCommand.Object));
        }

        [Test]
        [Description("If the user has selected a command, and then extra paramaters the extra paramaters should be parsed by the command.")]
        public async Task Run_ExtraArgumments_ProcessedByCommand()
        {
            //Given a single command
            var rootCommandGroup = new Mock<ICommandGroup>();
            var command = new Mock<ICommand>();

            command.Setup(p => p.CommandName).Returns("test");
            rootCommandGroup.Setup(p => p.Children).Returns(new List<ICommand>()
            {
                command.Object
            });

            //When we pass extra arguments but have selectd the command
            await SystemUnderTest.Run(new[] { "test", "something", "extra" }, rootCommandGroup.Object);

            //Then the command should process the next steps.
            command.Verify(c => c.Process(It.IsAny<string[]>(), It.IsAny<CancellationToken>()));
        }
        #endregion
    }
}

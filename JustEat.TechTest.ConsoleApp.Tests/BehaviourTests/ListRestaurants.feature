﻿Feature: ListRestaurants
	As a user running the application
	I can view a list of restaurants in a user submitted outcode (ex. SE19)
	So that I know which restaurants are currently available

	
Scenario: User Lists Restaurants For SE13
	Given a just eat console application
	When the user passes the following arguments 'restaurants list -o SE13'
	Then the application should display a list of restaurants


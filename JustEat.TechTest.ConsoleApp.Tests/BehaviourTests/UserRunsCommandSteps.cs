﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using FluentAssertions;
using TechTalk.SpecFlow;

namespace JustEat.TechTest.ConsoleApp.Tests.BehaviourTests
{

    [Binding]
    public class UserRunsCommandSteps
    {
        public StringBuilder OutputBuilder;
        public string Output;


        [Given("a just eat console application")]
        public void GivenAKnownOutcodeAndState()
        {
            OutputBuilder = new StringBuilder();
        }

        [When("the user passes the following arguments '(.*)'")]
        public void WhenTheyListRestauraunts(string args)
        {
            Console.SetOut(new StringWriter(OutputBuilder));

            Program.Main(args.Split(' '));
            Output = OutputBuilder.ToString();
        }

        [Then("the application should display a list of restaurants")]
        public void TheCountOpenShouldBe()
        {
            Output.Should().Contain(" restaurants found that are open now.");
        }

    }
}

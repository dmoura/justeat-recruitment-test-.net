﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using JustEat.TechTest.Api;
using JustEat.TechTest.Api.Exceptions;
using JustEat.TechTest.Api.Models;
using JustEat.TechTest.ConsoleApp.Commands.Restaurants;
using JustEat.TechTest.ConsoleApp.Commands.Restaurants.Presentation;
using Moq;
using NDesk.Options;
using NUnit.Framework;

namespace JustEat.TechTest.ConsoleApp.Tests.UnitTests
{
    [TestFixture]
    public class RestaurantsListCommandScenarios
    {
        RestaurantsListCommand SystemUnderTest { get; set; }

        Mock<IJustEatApiClient> MockApiClient { get; set; }
        Mock<IRestaurantsListCommandPresenter> MockPresenter { get; set; }
 
        [SetUp]
        public void Setup()
        {
            MockApiClient = new Mock<IJustEatApiClient>();
            MockPresenter = new Mock<IRestaurantsListCommandPresenter>();
            SystemUnderTest = new RestaurantsListCommand(MockApiClient.Object, MockPresenter.Object);
        }

        #region Test Cases
        [Test]
        [Description("When we run the resturant list command, it should print an error on screen if no outcode is present.")]
        public async Task Process_OutCodeNotPresent_ShowMessage()
        {
            //When we run the command but do not provide an outcode paramater
            await SystemUnderTest.Process(new string[] { });

            //The error should be presented
            MockPresenter.Verify(c => c.MissingOutcode());
            MockPresenter.Verify(c => c.ShowHelp(It.IsAny<OptionSet>(), It.IsAny<string>(), It.IsAny<string>()));
        }

        [Test]
        [Description("When we run the resturant list command, it should exit with status code 0, if no outcode is present.")]
        public async Task Process_OutcodeNotPresent_ErrorReturnCode()
        {
            //When we run the command but do not provide an outcode paramater
            var returnCode = await SystemUnderTest.Process(new string[] { });

            //We should return an error response code
            returnCode.Should().NotBe(0);
        }

        [Test]
        [Description("When we run the resturant list command with a valid outcode, then we should request the restaurants for the given outcode.")]
        public async Task Process_ValidOutcode_IsUsedWhenMakingRestRequest()
        {
            MockApiClient.Setup(a => a.GetRestaurantsAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(new RestaurantsCollection());

            //When we present valid outcodes
            await SystemUnderTest.Process(new string[] { "-o", "SE13" });

            //The outcode should be used for the request
            MockApiClient.Verify(a => a.GetRestaurantsAsync("SE13", It.IsAny<CancellationToken>()));
        }

        [Test]
        [Description("When we run the resturant list command with a valid outcode, the program executes sucessfully and should exit with a sucess status code.")]
        public async Task Process_ValidOutcode_ConsoleReturnsSucess()
        {
            MockApiClient.Setup(a => a.GetRestaurantsAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ReturnsAsync(new RestaurantsCollection()
            {
                Restaurants = new List<Restaurant>()
            });

            //When we present valid outcodes
            var returnCode = await SystemUnderTest.Process(new string[] { "-o", "SE13" });

            //We should return an valid return code.
            returnCode.Should().Be(0);
        }

        [Test]
        [Description("When we run the resturant list command with a valid outcode, but an exception was thrown, we should display the error.")]
        public async Task Process_RestClientHasException_ShowOnScreen()
        {
            //Given the server is expierning issues.
            MockApiClient.Setup(a => a.GetRestaurantsAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ThrowsAsync(new JustEatApiException("test", null));

            //When we present valid outcodes
            await SystemUnderTest.Process(new string[] { "-o", "SE13" });

            //The outcode should be used for the request
            MockPresenter.Verify(c => c.ShowError(It.IsAny<JustEatApiException>()));
        }

        [Test]
        [Description("When we run the resturant list command with a valid outcode, but an exception was thrown, we should exit with a bad return code.")]
        public async Task Process_RestClientReturns400HttpCode_ReturnErrorCodeToConsole()
        {
            //Given the server is expierning issues.
            MockApiClient.Setup(a => a.GetRestaurantsAsync(It.IsAny<string>(), It.IsAny<CancellationToken>())).ThrowsAsync(new JustEatApiException("test", null));

            //When we present valid outcodes
            var returnCode = await SystemUnderTest.Process(new string[] { "-o", "SE13" });

            //We should return an error response code
            returnCode.Should().NotBe(0);
        }
        #endregion
    }
}

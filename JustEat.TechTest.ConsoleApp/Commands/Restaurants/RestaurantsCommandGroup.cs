﻿using JustEat.TechTest.ConsoleUtils.Commands;

namespace JustEat.TechTest.ConsoleApp.Commands.Restaurants
{
    public class RestaurantsCommandGroup : AbstractCommandGroup
    {
        public RestaurantsCommandGroup(RestaurantsListCommand restaurantsList) : base("restaurants", "restaurants module.", restaurantsList)
        {
            
        }
    }
}

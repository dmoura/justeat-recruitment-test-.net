using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using JustEat.TechTest.Api;
using JustEat.TechTest.Api.Extensions;
using JustEat.TechTest.ConsoleApp.Commands.Restaurants.Presentation;
using JustEat.TechTest.ConsoleUtils.Commands;
using log4net;
using NDesk.Options;

namespace JustEat.TechTest.ConsoleApp.Commands.Restaurants
{
    public class RestaurantsListCommand : ICommand
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(RestaurantsListCommand).Name);

        private readonly IJustEatApiClient _justEatApiClient;
        private readonly IRestaurantsListCommandPresenter _presenter;

        public string CommandName => "list";
        public string Description => "displays a list of restaurants.";

        private string OutCode { get; set; }

        public RestaurantsListCommand(IJustEatApiClient justEatApiClient, IRestaurantsListCommandPresenter presenter)
        {
            Logger.Debug($"Initialized command {CommandName}.");

            _justEatApiClient = justEatApiClient;
            _presenter = presenter;
        }

        private OptionSet ProcessOptions(string[] args)
        {
            var options = new OptionSet
            {
                { "o|outcode=", "Outcode for the lookup for instance SE16",  v => { OutCode = v; } },
            };
            options.Parse(args);
            return options;
        }
        
        public async Task<int> Process(string[] args, CancellationToken ct = new CancellationToken())
        {
            Logger.Debug($"Processing command {CommandName}.");
            try
            {
                var options = ProcessOptions(args);

                if (string.IsNullOrEmpty(OutCode))
                {
                    Logger.Debug($"OutCode is missing for command {CommandName}.");

                    _presenter.MissingOutcode();
                    _presenter.ShowHelp(options, CommandName, Description);
                    return ConsoleReturnValues.GenericError;
                }
            
                var restaurants = await _justEatApiClient.GetRestaurantsAsync(OutCode, ct);
                var orderedRestaurants = restaurants.Restaurants.WhereIsOpenNow().OrderByStarRating().ToArray();

                _presenter.RenderResult(orderedRestaurants);

                return ConsoleReturnValues.Success;
            }
            //TODO: Dont trap all, handle each case explicitly
            catch (Exception ex)
            {
                Logger.Warn($"There was an issue processing the command {CommandName}.", ex);

                _presenter.ShowError(ex);

                return ConsoleReturnValues.GenericError;
            }
        }
    }
}

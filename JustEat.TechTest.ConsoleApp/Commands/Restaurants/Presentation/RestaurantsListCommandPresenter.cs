using System;
using System.Globalization;
using System.Linq;
using JustEat.TechTest.Api.Models;
using JustEat.TechTest.ConsoleUtils.Utils;
using NDesk.Options;

namespace JustEat.TechTest.ConsoleApp.Commands.Restaurants.Presentation
{
    public class RestaurantsListCommandPresenter : IRestaurantsListCommandPresenter
    {
        public void ShowHelp(OptionSet p, string commandName, string description)
        {
            Console.WriteLine($"{commandName} \t {description}");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }

        public void MissingOutcode()
        {
            Console.WriteLine("Please specify an outcode in your last paramater.");
            Console.WriteLine();
        }

        public void ShowError(Exception ex)
        {
            Console.Write($"Request failed due to {ex.Message}");
            Console.Write(".\n");
        }

        public void RenderResult(Restaurant[] restaurants)
        {
            using (var consoleTable = new ConsoleTable("Restaurant", "Cusine", "Rating"))
            {
                foreach (var restaurant in restaurants)
                {
                    var cusineTypes = string.Join(", ", restaurant.CuisineTypes.Select(ct => ct.Name));

                    consoleTable.PreProcessData(restaurant.Name, cusineTypes, restaurant.RatingStars.ToString(CultureInfo.InvariantCulture));
                }

                consoleTable.Write();
            }


            Console.WriteLine($"\n\n\n{restaurants.Length} restaurants found that are open now.");
        }
    }
}
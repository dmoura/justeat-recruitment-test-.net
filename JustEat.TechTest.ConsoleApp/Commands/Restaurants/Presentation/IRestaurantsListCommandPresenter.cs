using System;
using JustEat.TechTest.Api.Models;
using NDesk.Options;

namespace JustEat.TechTest.ConsoleApp.Commands.Restaurants.Presentation
{
    public interface IRestaurantsListCommandPresenter
    {
        void ShowHelp(OptionSet p, string commandName, string description);
        void MissingOutcode();
        void ShowError(Exception ex);
        void RenderResult(Restaurant[] restaurants);
    }
}
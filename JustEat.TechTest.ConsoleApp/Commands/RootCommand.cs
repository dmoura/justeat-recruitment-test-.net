﻿using JustEat.TechTest.ConsoleApp.Commands.Restaurants;
using JustEat.TechTest.ConsoleUtils.Commands;

namespace JustEat.TechTest.ConsoleApp.Commands
{
    public class RootCommand : AbstractCommandGroup
    {
        public RootCommand(RestaurantsCommandGroup restaurantsCommand) : base(restaurantsCommand)
        {
        }
    }
}

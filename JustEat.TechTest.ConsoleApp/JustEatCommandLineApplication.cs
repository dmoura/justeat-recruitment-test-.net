using JustEat.TechTest.ConsoleApp.Commands;
using JustEat.TechTest.ConsoleUtils.Commands;
using log4net;
using StructureMap;

namespace JustEat.TechTest.ConsoleApp
{
    public class JustEatCommandLineApplication
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(JustEatCommandLineApplication).Name);

        private readonly Registry _registry;

        public JustEatCommandLineApplication(Registry registry)
        {
            _registry = registry;
        }

        public int Run(string[] args)
        {
            Logger.Debug("Loading IoC");
            var ioc = new Container(_registry);
            var mainCommand = ioc.GetInstance<RootCommand>();
            var commandRunner = ioc.GetInstance<ICommandRunner>();

            Logger.Debug("Processing Commands");
            return commandRunner.Run(args, mainCommand).Result;
        }
    }
}
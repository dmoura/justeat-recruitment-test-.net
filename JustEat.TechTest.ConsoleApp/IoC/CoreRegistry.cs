﻿using System;
using JustEat.TechTest.Api;
using JustEat.TechTest.ConsoleApp.Commands.Restaurants.Presentation;
using JustEat.TechTest.ConsoleApp.Config;
using JustEat.TechTest.ConsoleUtils.Commands;
using RestSharp;
using RestSharp.Authenticators;
using StructureMap;

namespace JustEat.TechTest.ConsoleApp.IoC
{
    public class CoreRegistry : Registry
    {
        public CoreRegistry(IConfigReader configReader)
        {
            For<IRestClient>().Use<RestClient>().SelectConstructor(() => new RestClient());
            For<ICommandRunner>().Use<CommandRunner>();

            For<ICommandRunnerPresenter>().Use<ConsoleCommandRunnerPresenter>();
            For<IRestaurantsListCommandPresenter>().Use<RestaurantsListCommandPresenter>();
            
            For<IJustEatApiClient>().Use<RestSharpJustEatApiClient>()
                .Ctor<Uri>("baseUri").Is(new Uri(configReader.ReadModuleKey("Api.Endpoint").AsString()))
                .Ctor<IAuthenticator>().Is(new HttpBasicAuthenticator(configReader.ReadModuleKey("Api.Username").AsString(), configReader.ReadModuleKey("Api.Password").AsString()))
                .Ctor<string>("language").Is(configReader.ReadModuleKey("Api.Language").AsString())
                .Ctor<string>("tenant").Is(configReader.ReadModuleKey("Api.Tenant").AsString());
        }
    }
}

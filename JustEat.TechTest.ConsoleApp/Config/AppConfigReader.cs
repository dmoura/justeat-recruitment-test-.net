﻿using System.Configuration;
using log4net;

namespace JustEat.TechTest.ConsoleApp.Config
{
    public class AppConfigReader : IConfigReader
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AppConfigReader).Name);

        private readonly string _module;

        public AppConfigReader(string module)
        {
            _module = module;
        }

        public ConfigurationValue ReadModuleKey(string key)
        {
            return ReadKey($"{_module}.{key}");
        }

        public ConfigurationValue ReadKey(string key)
        {
            var value = new ConfigurationValue(ConfigurationManager.AppSettings[key]);
            Logger.Debug($"Configuration Param Loaded: {key} = {value}");
            return value;
        }
    }
}
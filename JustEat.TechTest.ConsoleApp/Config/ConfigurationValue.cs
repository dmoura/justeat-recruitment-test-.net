namespace JustEat.TechTest.ConsoleApp.Config
{
    public class ConfigurationValue
    {
        private readonly string _value;

        public ConfigurationValue(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return AsString();
        }

        public int AsInt()
        {
            return int.Parse(_value);
        }

        public string AsString()
        {
            return _value;
        }

        public bool AsBool()
        {
            return bool.Parse(_value);
        }
    }
}
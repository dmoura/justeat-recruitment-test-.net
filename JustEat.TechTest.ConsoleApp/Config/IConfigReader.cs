﻿namespace JustEat.TechTest.ConsoleApp.Config
{
    public interface IConfigReader
    {
        ConfigurationValue ReadModuleKey(string key);
        ConfigurationValue ReadKey(string key);
    }
}

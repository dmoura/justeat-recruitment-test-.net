﻿using JustEat.TechTest.ConsoleApp.Config;
using JustEat.TechTest.ConsoleApp.IoC;

namespace JustEat.TechTest.ConsoleApp
{
    public class Program
    {
        private const string ModuleName = "JustEat.Cmd";
        public static int Main(string[] args)
        {
            var justCommandLine = new JustEatCommandLineApplication(new CoreRegistry(new AppConfigReader(ModuleName)));
            return justCommandLine.Run(args);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using JustEat.TechTest.Api.Models;

namespace JustEat.TechTest.Api.Extensions
{
    /// <summary>
    /// Provides some sugar for resuable functionality for the restaurants model.
    /// </summary>
    public static class RestaurantsExtensions
    {
        /// <summary>
        /// Filters a collection of restaurants by those which are currently open.
        /// </summary>
        /// <param name="restaurants">A collection of resturaunts that may currently not be filtered.</param>
        /// <returns>IEnumerable<Restaurant></returns>
        public static IEnumerable<Restaurant> WhereIsOpenNow(this IEnumerable<Restaurant> restaurants)
        {
            return restaurants.Where(r => r.IsOpenNow);
        }

        /// <summary>
        /// Orders a resturants collection in descending order of resturants by their rating.
        /// </summary>
        /// <param name="restaurants">A collection of resturaunts that may currently not be ordered.</param>
        /// <returns>IEnumerable<Restaurant></returns>
        public static IEnumerable<Restaurant> OrderByStarRating(this IEnumerable<Restaurant> restaurants)
        {
            return restaurants.OrderBy(r => -r.RatingStars);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JustEat.TechTest.Api.Models
{
    public class RestaurantDeal
    {
        public string Description { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal QualifyingPrice { get; set; }
    }
}

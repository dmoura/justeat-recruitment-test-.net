﻿using System.Collections.Generic;
using System.Linq;

namespace JustEat.TechTest.Api.Models
{

    public class RestaurantsCollection : JustEatApiResource
    {
        public List<Restaurant> Restaurants { get; set; }
        public string ShortResultText { get; set; }
        public string Area { get; set; }


    }
}

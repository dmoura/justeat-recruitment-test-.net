﻿namespace JustEat.TechTest.Api.Models
{
    public class ApiError
    {
        public string ErrorType { get; set; }
        public string Message { get; set; }
    }
}
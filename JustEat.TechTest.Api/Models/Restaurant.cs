﻿using System;
using System.Collections.Generic;

namespace JustEat.TechTest.Api.Models
{
    public class Restaurant
    {
        public double Score { get; set; }
        public double? DriveDistance { get; set; }
        public bool DriveInfoCalculated { get; set; }
        public DateTime NewnessDate { get; set; }
        public long? DeliveryMenuId { get; set; }
        public DateTime? DeliveryOpeningTime { get; set; }
        public decimal? DeliveryCost { get; set; }
        public DateTime? OpeningTime { get; set; }

        //Either OpeningTime or OpeningTimeIso not  required..
        public DateTime? OpeningTimeIso { get; set; }
        public decimal? MinimumDeliveryValue { get; set; }

        //Notes: all are null, assuming timespan.
        public TimeSpan? DeliveryTime { get; set; }

        public bool SendsOnItsWayNotifications { get; set; }

        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }
        public List<CuisineType> CuisineTypes { get; set; }

        public Uri Url { get; set; }
        public bool IsOpenNow { get; set; }
        public bool IsSponsored { get; set; }
        public bool IsNew { get; set; }
        public bool IsTemporarilyOffline { get; set; }
        public string ReasonWhyTemporarilyOffline { get; set; }
        public string UniqueName { get; set; }
        public bool IsCloseBy { get; set; }
        public bool IsHalal { get; set; }
        public int DefaultDisplayRank { get; set; }
        public bool IsOpenNowForDelivery { get; set; }
        public double RatingStars { get; set; }
        public List<RestaurantLogo> Logo { get; set; }
        public List<RestaurantDeal> Deals { get; set; }
        public int NumberOfRatings { get; set; }
    }
}

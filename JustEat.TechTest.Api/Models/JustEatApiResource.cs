﻿using System.Collections.Generic;

namespace JustEat.TechTest.Api.Models
{
    public abstract class JustEatApiResource
    {
        public List<ApiError> Errors { get; set; }
        public bool HasErrors { get; set; }
    }
}
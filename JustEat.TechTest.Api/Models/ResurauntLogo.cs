﻿using System;

namespace JustEat.TechTest.Api.Models
{
    public class RestaurantLogo
    {
        public Uri StandardResolutionUrl { get; set; }
    }
}

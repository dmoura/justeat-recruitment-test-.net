﻿using System;

namespace JustEat.TechTest.Api.Exceptions
{
    public class HttpJustEatApiException : JustEatApiException
    {
        public int StatusCode { get; set; }

        public HttpJustEatApiException(int statusCode, string message, Exception innerException = null) : base(message, innerException)
        {
            StatusCode = statusCode;
        }
    }
}
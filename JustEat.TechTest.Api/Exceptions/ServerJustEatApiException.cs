﻿using System;

namespace JustEat.TechTest.Api.Exceptions
{
    public class ServerJustEatApiException : HttpJustEatApiException
    {
        public ServerJustEatApiException(int statusCode, string message, Exception innerException = null) : base(statusCode, message, innerException)
        {

        }
    }
}
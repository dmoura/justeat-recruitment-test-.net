﻿using System;

namespace JustEat.TechTest.Api.Exceptions
{
    public class JustEatApiException : Exception
    {
        public JustEatApiException(string message, Exception innerException) : base(message, innerException)
        {
            
        }
    }
}

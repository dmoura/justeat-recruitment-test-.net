﻿using System;

namespace JustEat.TechTest.Api.Exceptions
{
    public class ClientJustEatApiException : HttpJustEatApiException
    {
        public ClientJustEatApiException(int statusCode, string message, Exception innerException = null) : base(statusCode, message, innerException)
        {

        }
    }
}
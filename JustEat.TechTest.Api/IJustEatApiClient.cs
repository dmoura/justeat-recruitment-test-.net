﻿using System.Threading;
using System.Threading.Tasks;
using JustEat.TechTest.Api.Models;

namespace JustEat.TechTest.Api
{
    public interface IJustEatApiClient
    {
        Task<RestaurantsCollection> GetRestaurantsAsync(string query = null, CancellationToken ct = default(CancellationToken));
    }
}
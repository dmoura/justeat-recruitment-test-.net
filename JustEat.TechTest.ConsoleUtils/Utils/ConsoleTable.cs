﻿using System;
using System.Collections.Generic;

namespace JustEat.TechTest.ConsoleUtils.Utils
{
    /// <summary>
    /// This class is responsible processing output to the console in the form of a table.
    /// Needs to be refactored for testability.
    /// </summary>
    public class ConsoleTable : IDisposable
    {
        private readonly int _cols;
        private List<string>[] _data;
        private int[] _maxLengths;
        private string[] _headings;

        public ConsoleTable(params string[] headings)
        {
            _headings = headings;
            _cols = _headings.Length;

            _data = new List<string>[_cols];
            _maxLengths = new int[_cols];

            for (int i = 0; i < _cols; i++)
            {
                if (_headings[i].Length > _maxLengths[i]) _maxLengths[i] = _headings[i].Length;

                _data[i] = new List<string>();
            }
        }


        public void PreProcessData(params string[] colsData)
        {
            if (colsData.Length > _cols)
            {
                //Todo: Improve data
                throw new Exception("Invalid number of cols requested");
            }

            for (int i = 0; i < _cols; i++)
            {
                if (colsData.Length > i)
                {
                    var colData = colsData[i];
                    if (colData.Length > _maxLengths[i]) _maxLengths[i] = colData.Length;
                    _data[i].Add(colData);
                }
                else
                {
                    _data[i].Add("");
                }
            }
        }

        public void Write()
        {
            var rowCount = _data[0].Count;

            for (int i = 0; i < _cols; i++)
            {
                Console.Write(_headings[i].PadRight(_maxLengths[i]));
                if (i != _cols - 1) Console.Write("\t");
            }

            Console.Write("\n");

            for (int i = 0; i < _cols; i++)
            {
                Console.Write(new string('-', _maxLengths[i]));
                if (i != _cols - 1) Console.Write("\t");
            }
            Console.Write("\n");


            for (int j = 0; j < rowCount; j++)
            {
                for (int i = 0; i < _cols; i++)
                {
                    Console.Write(_data[i][j].PadRight(_maxLengths[i]));
                    if (i != _cols - 1) Console.Write("\t");
                }

                Console.Write("\n");
            }
        }

        public void Dispose()
        {
            _data = null;
            _headings = null;
            _maxLengths = null;
        }
    }
}

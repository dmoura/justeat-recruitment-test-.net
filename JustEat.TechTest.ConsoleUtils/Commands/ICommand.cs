﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace JustEat.TechTest.ConsoleUtils.Commands
{
    public interface ICommand
    {
        string CommandName { get; }
        string Description { get; }

        //TODO: not used for command group - improve this
        Task<int> Process(string[] args, CancellationToken ct = new CancellationToken());
    }
}
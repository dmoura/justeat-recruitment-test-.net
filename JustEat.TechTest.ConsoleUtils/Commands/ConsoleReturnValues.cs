namespace JustEat.TechTest.ConsoleUtils.Commands
{
    public struct ConsoleReturnValues
    {
        public const int GenericError = 1;
        public const int Success = 0;
    }
}
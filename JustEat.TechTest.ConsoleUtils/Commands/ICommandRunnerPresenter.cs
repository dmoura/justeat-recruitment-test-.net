﻿namespace JustEat.TechTest.ConsoleUtils.Commands
{
    public interface ICommandRunnerPresenter
    {
        void CommandNotFound(string commandName);

        void PleaseSelectSubCommand();

        void ListSubCommands(ICommandGroup lastRoot);
    }
}

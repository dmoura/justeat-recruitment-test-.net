﻿using System.Collections.Generic;

namespace JustEat.TechTest.ConsoleUtils.Commands
{
    public interface ICommandGroup : ICommand
    {
        ICollection<ICommand> Children { get; }
    }
}
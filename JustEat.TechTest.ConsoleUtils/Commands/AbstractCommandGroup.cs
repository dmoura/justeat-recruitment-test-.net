﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace JustEat.TechTest.ConsoleUtils.Commands
{
    public abstract class AbstractCommandGroup : ICommandGroup
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AbstractCommandGroup).Name);
        public string CommandName { get; protected set; }
        public string Description { get; protected set; }

        public ICollection<ICommand> Children { get; private set; } = new  List<ICommand>();

        /// <summary>
        /// root command constructor
        /// </summary>
        /// <param name="commands"></param>
        protected AbstractCommandGroup(params ICommand[] commands)
        {
            Logger.Debug($"Initializing Command {CommandName}");

            foreach (var command in commands)
            {
                Children.Add(command);
            }
        }

        protected AbstractCommandGroup(string commandName, string description, params ICommand[] commands)
        {
            CommandName = commandName;
            Description = description;
            
            AddCommands(commands);
        }

        protected void AddCommand(ICommand command)
        {
            Logger.Debug($"Command {CommandName} is adding child {command.CommandName}");

            Children.Add(command);
        }


        protected void AddCommands(ICommand[] commands)
        {
            foreach (var command in commands)
            {
                AddCommand(command);
            }
        }

        public virtual async Task<int> Process(string[] args, CancellationToken ct = new CancellationToken()) => ConsoleReturnValues.GenericError;
    }
}
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NDesk.Options;

namespace JustEat.TechTest.ConsoleUtils.Commands
{
    /// <summary>
    /// The command runner is responsible for processing console arguments and delecating them to the right command.
    /// </summary>
    public class CommandRunner : ICommandRunner
    {

        private readonly ICommandRunnerPresenter _presenter;

        public CommandRunner(ICommandRunnerPresenter presenter)
        {
            _presenter = presenter;
        }

        public async Task<int> Run(string[] args, ICommand rootCommand, CancellationToken ct = new CancellationToken())
        {
            var options = new OptionSet();
            var subCommands = options.Parse(args);
            
            var currentRoot = rootCommand;

            //Get all arguments which arent switched (-)
            foreach (var command in subCommands.Where(sc => !sc.StartsWith("-")))
            {
                var lastRoot = currentRoot as ICommandGroup;

                //last root was not a command group so lets try and run it..
                if (lastRoot == null)
                {
                    break;
                }

                currentRoot = (currentRoot as ICommandGroup)?.Children.FirstOrDefault(c => c.CommandName.Equals(command, StringComparison.OrdinalIgnoreCase));

                //If we have requested a sub command that doesnt exist..
                if (currentRoot == null)
                {
                    _presenter.CommandNotFound(command);
                    _presenter.ListSubCommands(lastRoot);

                    return ConsoleReturnValues.GenericError;
                }
            }

            //Either the user has selected a command group or a command at this point, depending which we should either list sub commands or process the request.
            var commandGroup = currentRoot as ICommandGroup;
            if (commandGroup != null)
            {
                _presenter.PleaseSelectSubCommand();
                _presenter.ListSubCommands(commandGroup);
            }
            else
            {
                await currentRoot.Process(args, ct).ConfigureAwait(false);
            }

            return ConsoleReturnValues.Success;
        }
    }
}
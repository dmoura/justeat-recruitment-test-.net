﻿using System;
using JustEat.TechTest.ConsoleUtils.Utils;

namespace JustEat.TechTest.ConsoleUtils.Commands
{
    public class ConsoleCommandRunnerPresenter : ICommandRunnerPresenter
    {
        public void CommandNotFound(string commandName)
        {
            Console.WriteLine($"{commandName} is not a valid sub command.");
        }

        public void PleaseSelectSubCommand()
        {
            Console.WriteLine("Please choose a sub command from the list");
            Console.WriteLine();
        }

        public void ListSubCommands(ICommandGroup lastRoot)
        {
            using (var consoleTable = new ConsoleTable("Sub Command", "Description"))
            {
                foreach (var childCommand in lastRoot.Children)
                {
                    consoleTable.PreProcessData(childCommand.CommandName, childCommand.Description);
                }

                consoleTable.Write();
            }
        }
    }
}
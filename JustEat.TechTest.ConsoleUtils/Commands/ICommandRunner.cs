using System.Threading;
using System.Threading.Tasks;

namespace JustEat.TechTest.ConsoleUtils.Commands
{
    public interface ICommandRunner
    {
        Task<int> Run(string[] args, ICommand rootCommand, CancellationToken ct = new CancellationToken());
    }
}